const express = require('express')
const app = express()
//con fs cogemos ficheros
const fs = require('fs') //para el log
const path = require('path') //para recursos estatticos
const morgan= require('morgan')

//para midleware
//app.use((req, res, next)=>{
    //operaciones del middleware
    //res.send("hasta aqui!!")
    //next() //para ir al siguiente middleware o a la ruta
    // también podríamos hacer un send() y cortar
    // la cola de middlewares, por ej en un control de permisos
    
  //})

  //middleware de logg
  app.use(morgan('dev'))
  app.use(function (req, res, next) {
    var now = new Date().toString()
    var log = `${now}: ${req.method} ${req.url}`
    console.log(log)
    fs.appendFile('server.log', `${log}\n`, (err) => {
      if (err) console.log(`No se ha podido usar el fichero de log:  ${err}`)
    })
    next()
  })

  //pàra crear contenido estatico
  //sataticRoute mira donde estam nuestro directorio(public) y le agrega una carpeta public
const publicRoute = path.join(__dirname, 'public')
app.use(express.static(publicRoute))
//para contenido estatico
const staticRoute = path.join(__dirname, 'estatico')
app.use('/static', express.static(staticRoute))

app.get('/', (req, res) => {
    res.send('Hola Mundo....')
})
app.listen(3000, (err)=> {
    console.log("estamos en el puerto 3000");
})
/*
app.listen(3000, () => {
    console.log('Servidor web arrancado en el puerto 3000')
  })
  */
 const contacto = require('./nombres2.js');
 app.get('/contactar', (req, res) => {
    res.render('contactar.hbs', {
        pageTitle: 'Contactar',
        currentYear: new Date().getFullYear()
      })
})


//para el template hbs
const hbs = require('hbs')
hbs.registerPartials(path.join(__dirname, 'views', 'partials'))
app.set('view engine', 'hbs'); // clave valor